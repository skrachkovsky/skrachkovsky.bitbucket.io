/**
 * @author Sergei Krachkovsky
 * @email  sergei.krachkovsky@hotmail.com
 */

var MapId = {
    body: '#map-id-body',
    hoverClass: 'el-hover',
    previewClass: 'el-preview',
    objectClasses: [],
    defClasses: [
        'fil2',
        'fil3',
        'fil5'
    ],
    url: {
        getBoutique: '',
        saveMap: '',
        setBoutiqueMapId: ''
    },
    mallId: '',
    boutiques: {},
    init: function () {
        if (localStorage.getItem('hide-labels')) {
            $(MapId.body + ' .show-labels').attr('checked', false);
        } else {
            $(MapId.body + ' .show-labels').attr('checked', true);
        }
        $(MapId.body + ' .show-labels').click(function () {
            if (localStorage.getItem('hide-labels')) {
                localStorage.removeItem('hide-labels');
                $(MapId.body + ' .show-labels').attr('checked', true);
            } else {
                localStorage.setItem('hide-labels', true);
                $(MapId.body + ' .show-labels').attr('checked', false);
            }
            MapId.updateLabels();
        });
        $(MapId.body + ' .get-max-id').click(function () {
            alert(MapId.getMaxId());
        });
        $(MapId.body + ' .clear-all').click(function () {
            $(MapId.body + ' .modal .map-content svg').find('path, polygon').each(function () {
                if ($(this).attr('id') && $(this).attr('id').match(/^_\d+/)) {
                    $(this).removeAttr('id');
                }
            });
            MapId.updateLabels();
        });
        $(MapId.body + ' .auto-id').click(function () {
            var fl = Math.abs(parseInt($(MapId.body + ' .first-number').val()));
            if (fl < 1) {
                alert('Firs number invalid');
                return;
            }
            var tpl = fl.toString() + '00';
            var min = 0;
            var or = $(MapId.body + ' .override-all').is(':checked');
            if (!or) {
                min = parseInt(MapId.getMaxId().toString().substr(fl.toString().length));
                if (isNaN(min) || min < 0) {
                    min = 0;
                }
            }
            $(MapId.body + ' .modal .map-content svg').find('path, polygon').each(function () {
                if ($(this).attr('id') && $(this).attr('id').match(/^_\d{3,}$/)) {
                    if (or) {
                        $(this).removeAttr('id');
                    } else {
                        return;
                    }
                }
                if (MapId.hasObjectClass(this)) {
                    min++;
                    if (min > (parseInt(1 + tpl.substr(fl.toString().length)) - 1)) {
                        tpl += '0';
                        min = 1;
                    }
                    $(this).attr('id', '_' + tpl.substr(0, tpl.length - min.toString().length) + min.toString());
                }
            });
            MapId.updateLabels();
        });
        $(MapId.body + ' .map-content').on('scrollstart', function () {
            MapId.removeLabels();
            if ($(MapId.body).next().hasClass('popover')) {
                $(MapId.body).next().css('visibility', 'hidden');
            }
        }).on('scrollstop', function () {
            MapId.updateLabels();
            if ($(MapId.body).next().hasClass('popover')) {
                $(MapId.body).next().css('visibility', 'visible');
                MapId.updatePopUp();
            }
        });
        $(MapId.body + ' .save-svg').click(function () {
            MapId.unbindActions();
            var data = MapId.output();
            // todo before save
            $('#output').val(data);
            var xhr = new XMLHttpRequest();
            var boundary = parseInt(Math.random() * 1e11);
            var body =
                '--' + boundary + '\r\n' +
                'Content-Disposition: form-data; name="file"; filename="output.svg"\r\n' +
                'Content-Type: application/octet-stream\r\n' +
                '\r\n' + data + '\r\n' +
                '--' + boundary;
            xhr.open('POST', MapId.url.saveMap, true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    alert('Saved!');
                }
            };
            xhr.setRequestHeader('Content-type', 'multipart/form-data; boundary="' + boundary + '"');
            xhr.setRequestHeader('Connection', 'close');
            xhr.setRequestHeader('Content-length', body.length);
            xhr.send(body);
            MapId.bindActions();
        });
    },
    loadFile: function (file) {
        $.ajax({
            url: file,
            dataType: 'text',
            success: function (r) {
                MapId.display(r);
            },
            error: function () {
                alert('Error');
            }
        })
    },
    display: function (r) {
        $(MapId.body + ' .modal .map-content').html('<div class="svg-block">' + r + '</div>');
        var svg = $(MapId.body + ' .modal .map-content svg');
        if (svg.size()) {
            $(MapId.body + ' .modal').modal('show');
            var ww = $(MapId.body + ' .modal .map-content').width();
            var sw = svg.width();
            var sh = svg.height();
            svg.css('width', ww + 'px');
            svg.css('height', (sh * ww / sw) + 'px');
            MapId.objectClasses = [];
            MapId.activeClasses();
            MapId.updateLabels();
            MapId.bindActions();
            var fn = 1;
            $(MapId.body + ' .modal .map-content svg').find('path, polygon').each(function () {
                if ($(this).attr('id') && $(this).attr('id').match(/^_\d{3,}$/)) {
                    var v = parseInt($(this).attr('id').substr(1, 1));
                    if (v > fn) {
                        fn = v;
                    }
                }
            });
            $(MapId.body + ' .first-number').val(fn);
        } else {
            alert('Error: no svg');
        }
    },
    bindActions: function () {
        MapId.unbindActions();
        $(MapId.body + ' .modal .map-content svg').find('path, polygon').bind('click', function () {
            var _this = this;
            var id = $(this).attr('id') ? parseInt($(this).attr('id').substr(1)) : '';
            if (!id) {
                id = '';
            }
            var opts = '';
            for (var i = 0, b; b = MapId.boutiques[i]; i++) {
                opts += '<option value="' + b.id + '"' + (b.map_item_id == id ? ' selected' : '') + '>' + b.name + '</option>';
            }
            MapId.popUp(
                this,
                '<form class="setup-id">' +
                    '<div class="form-group"><input placeholder="Map ID" class="form-control" value="' + id + '" type="number" min="1" name="id"/></div>' +
                    '<div class="form-group">' +
                        '<select class="form-control" name="boutique">' +
                            '<option value="">-No Boutique-</option>' + opts +
                        '</select>' +
                    '</div>' +
                    '<button class="btn btn-primary btn-block" type="submit">Сохранить</button>' +
                '</form>'
            );
            $(MapId.body).next().find('form.setup-id').unbind().bind('submit', function () {
                var sid = $(this).find('input[name="id"]').val().trim();
                if (sid) {
                    $(_this).attr('id', '_' + sid);
                } else {
                    $(_this).removeAttr('id');
                }
                MapId.updateLabels();
                var bid = $(this).find('select[name="boutique"]').val();
                for (var i = 0, b; b = MapId.boutiques[i]; i++) {
                    if (b.map_item_id == sid) {
                        b.map_item_id = '';
                    }
                    if (b.id == bid) {
                        b.map_item_id = sid;
                    }
                }
                $.ajax({
                    url: MapId.url.setBoutiqueMapId,
                    type: 'POST',
                    data: {
                        mall_id: MapId.mallId,
                        boutique_id: bid,
                        map_id: sid
                    },
                    complete: function () {
                        MapId.popUp(false);
                    }
                });
                return false;
            });
        }).bind('mouseover', function() {
            if (MapId.hasObjectClass(this)) {
                $(this).attr(MapId.hoverClass, true);
            }
        }).bind('mouseout', function () {
            $(this).removeAttr(MapId.hoverClass);
        });
    },
    unbindActions: function () {
        $(MapId.body + ' .modal .map-content svg').find('path, polygon')
            .unbind()
            .removeAttr(MapId.hoverClass);
    },
    hasObjectClass: function (el) {
        var ex = false;
        for (var i = 0, k; k = MapId.objectClasses[i]; i++) {
            var reg = new RegExp('(^|\\s)(' + k + ')(\\s|$)');
            if ($(el).attr('class').match(reg)) {
                delete reg;
                ex = true;
                break;
            }
            delete reg;
        }
        return ex;
    },
    removeLabels: function () {
        $(MapId.body + ' .modal .map-labels').html('');
    },
    updateLabels: function () {
        MapId.removeLabels();
        if (localStorage.getItem('hide-labels')) {
            return;
        }
        var svg = $(MapId.body + ' .modal .map-content svg');
        var pos = $(MapId.body + ' .map-content').offset();
        if (svg.size()) {
            var html = '';
            svg.find('path, polygon').each(function () {
                if ($(this).attr('id') && $(this).attr('id').match(/^_\d{3,}$/)) {
                    var of = $(this).offset();
                    of.left -= pos.left;
                    of.top -= pos.top;
                    html += '<div class="label" data-id="' + $(this).attr('id') + '" style="left: ' + of.left + 'px; top: ' + of.top + 'px;">' + $(this).attr('id').substr(1) + '</div>';
                }
            });
            $(MapId.body + ' .modal .modal-content .map-labels').html(html);
        }
    },
    output: function () {
        var h = $(MapId.body + ' .modal .modal-content .map-content .svg-block').html();
        h = h.replace(/<!--\?xml(.+?)\?-->/, '<?xml$1?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">');
        h = h.replace(/>\n+</g, '>\n<');
        h = h.replace(/^\s+/gm, '');
        h = h.replace(/(<style.+?>)/gi, '$1\n<![CDATA[');
        h = h.replace(/(<\/style>)/gi, ']]>\n$1');
        return h
    },
    __prevEl: null,
    popUp: function (el, html) {
        if (MapId.__prevEl && MapId.__prevEl == el) {
            MapId.__prevEl = null;
            $(MapId.body).popover('hide');
            return;
        }
        MapId.__prevEl = el;
        $(MapId.body).popover({
            html: true,
            content: html,
            placement: 'top',
            trigger: 'manual'
        });
        $(MapId.body).popover('show');
        $(MapId.body).next().data('el', el);
        MapId.updatePopUp();
    },
    updatePopUp: function () {
        if (!$(MapId.body).next().hasClass('popover')) {
            return;
        }
        var el = $(MapId.body).next().data('el');
        if (!el) {
            return;
        }
        $(MapId.body).next().css({
            top: $(el).offset().top - $(MapId.body).next().outerHeight() + 'px',
            left: $(el).offset().left + el.getBoundingClientRect().width / 2 - $(MapId.body).next().outerWidth() / 2 + 'px'
        });
    },
    activeClasses: function () {
        var cls = {};
        $(MapId.body + ' .modal .map-active-cls').html('');
        $(MapId.body + ' .modal .modal-content .map-content svg').find('polygon, path').each(function () {
            var hc = $(this).attr('class').split(' ');
            for (var i = 0, k; k = hc[i]; i++) {
                if (!cls[k]) {
                    cls[k] = 1;
                } else {
                    cls[k]++;
                }
            }
        });
        var html = '';
        for (var key in cls) {
            var ex = false;
            if (MapId.objectClasses.length == 0) {
                $.extend(MapId.objectClasses, MapId.defClasses);
            }
            for (var i = 0, k; k = MapId.objectClasses[i]; i++) {
                if (key == k) {
                    ex = true;
                    break;
                }
            }
            html +=
                '<label class="checkbox-inline" data-cls="' + key + '">' +
                    '<input type="checkbox" name="activeCls[]"' + (ex ? ' checked' : '') + '/> ' +
                    key + ' <span class="badge">' + cls[key] + '</span>' +
                '</label> ';
        }
        $(MapId.body + ' .modal .map-active-cls').html(html);
        $(MapId.body + ' .modal .map-active-cls input').unbind()
            .bind('change', function () {
                var cls = $(this).parent().data('cls');
                var ex = false;
                for (var i = 0, k; k = MapId.objectClasses[i]; i++) {
                    if (k == cls) {
                        ex = true;
                        break;
                    }
                }
                if (ex) {
                    MapId.objectClasses = MapId.arrayRemove(MapId.objectClasses, i);
                } else {
                    MapId.objectClasses.push(cls);
                }
            });
        $(MapId.body + ' .modal .map-active-cls label').unbind()
            .bind('mouseover', function () {
                var cls = $(this).data('cls');
                $(MapId.body + ' .modal .modal-content .map-content svg').find('polygon.' + cls + ', path.' + cls).attr(MapId.previewClass, true);
            })
            .bind('mouseout', function () {
                $(MapId.body + ' .modal .modal-content .map-content svg').find('polygon, path').removeAttr(MapId.previewClass);
            });

    },
    arrayRemove: function (arr, index) {
        return arr.slice(0, index).concat(arr.slice(index + 1, arr.length))
    },
    getMaxId: function () {
        var max = 0;
        $(MapId.body + ' .modal .map-content svg').find('path, polygon').each(function () {
            if ($(this).attr('id') && $(this).attr('id').match(/^_\d{3,}$/)) {
                var n = parseInt($(this).attr('id').substr(1));
                if (n > max) {
                    max = n;
                }
            }
        });
        return max;
    }
};